# oorco-tds

Servidor Thredds sobre linux alpine para el grupo OORCO

## Construir la imagen del contenedor

```bash
$ docker build -t oorco-tds:latest .

```
## Inicio rapido

```bash
$ docker run -it -p 8080:8080 oorco-tds:latest
```


Ahora abrir en el explorador la ruta http://localhost:8080/thredds/ para acceder a la interface web del servidor Thredds


 
