FROM tomcat:9.0.8-jre8-alpine

###
# Usual maintenance for tomcat
###
RUN apk update && \
    apk --no-cache add bash linux-pam shadow curl su-exec zip sed && \
    ###
    # Cleanup apt
    ###
    rm -rf /var/cache/apk/* && \
    ###
    # Eliminate default web applications
    ###
    rm -rf ${CATALINA_HOME}/work && \
    rm -rf ${CATALINA_HOME}/logs && \
    rm -rf ${CATALINA_HOME}/webapps/* && \
    rm -rf ${CATALINA_HOME}/server/webapps/* && \
    ###
    # Obscuring server info
    ###
    mkdir -p ${CATALINA_HOME}/lib/org/apache/catalina/util/ && \
    unzip ${CATALINA_HOME}/lib/catalina.jar \
        org/apache/catalina/util/ServerInfo.properties \
        -d ${CATALINA_HOME}/lib/org/apache/catalina/util/ && \
    mv ${CATALINA_HOME}/lib/org/apache/catalina/util/org/apache/catalina/util/ServerInfo.properties ${CATALINA_HOME}/lib/org/apache/catalina/util/ && \
    sed -i 's/server.info=.*/server.info=Apache Tomcat/g' \
        ${CATALINA_HOME}/lib/org/apache/catalina/util/ServerInfo.properties && \
    zip -ur ${CATALINA_HOME}/lib/catalina.jar \
        ${CATALINA_HOME}/lib/org/apache/catalina/util/ServerInfo.properties && \
    rm -rf ${CATALINA_HOME}/lib/org && \
    sed -i 's/<Connector/<Connector server="Apache" secure="true"/g' \
        ${CATALINA_HOME}/conf/server.xml && \
    ###
    # Ugly, embarrassing, fragile solution to adding the CredentialHandler
    # element until we get XSLT or the equivalent figured out. True for other
    # XML manipulations herein.
    # https://github.com/Unidata/tomcat-docker/issues/27
    # https://stackoverflow.com/questions/32178822/tomcat-understanding-credentialhandler
    ## 
    sed -i 's/resourceName="UserDatabase"\/>/resourceName="UserDatabase"><CredentialHandler className="org.apache.catalina.realm.MessageDigestCredentialHandler" algorithm="SHA" \/><\/Realm>/g' \
        ${CATALINA_HOME}/conf/server.xml

###
# Setting restrictive umask container-wide, available for alpine linux?
###
#RUN echo "session optional pam_umask.so" >> /etc/pam.d/common-session && \
#    sed -i 's/UMASK.*022/UMASK           007/g' /etc/login.defs

###
# Thredds
###

RUN mkdir /downloads
WORKDIR /downloads

###
# Installing netcdf-c library according to:
# http://www.unidata.ucar.edu/software/thredds/current/netcdf-java/reference/netcdf4Clibrary.html
###

ENV LD_LIBRARY_PATH /usr/local/lib:${LD_LIBRARY_PATH} 
ENV HDF5_VERSION 1.8.20
ENV ZLIB_VERSION 1.2.8
ENV NETCDF_VERSION 4.6.0
ENV ZDIR /usr/local
ENV H5DIR /usr/local
ENV PDIR /usr

#apk dependencies for building packages
RUN apk add --update --virtual build-dependencies build-base gcc m4 curl-dev

#zlib dependency
RUN curl ftp://ftp.unidata.ucar.edu/pub/netcdf/netcdf-4/zlib-${ZLIB_VERSION}.tar.gz | tar xz && \
    cd zlib-${ZLIB_VERSION} && \
    ./configure --prefix=/usr/local && \
    make && make install

ENV HDF5_VER hdf5-${HDF5_VERSION}
ENV HDF5_FILE ${HDF5_VER}.tar.gz

#hdf5 dependency
RUN curl https://support.hdfgroup.org/ftp/HDF5/releases/${HDF5_VER%.*}/${HDF5_VER}/src/${HDF5_FILE} | tar xz && \
    cd hdf5-${HDF5_VERSION} && \
    ./configure --with-zlib=${ZDIR} --prefix=${H5DIR} --enable-threadsafe --with-pthread=${PDIR} --enable-unsupported --prefix=/usr/local && \
    make && make install && make check-install && ldconfig /usr/local/lib
#RUN cd hdf5-${HDF5_VERSION} && make 
#RUN cd hdf5-${HDF5_VERSION} && make install 
#RUN cd hdf5-${HDF5_VERSION} && make check-install 
#RUN ldconfig /usr/local/lib
# make && make check && make install && make check-install && ldconfig

#netCDF4-c
RUN export CPPFLAGS=-I/usr/local/include \
    LDFLAGS=-L/usr/local/lib && \
    curl ftp://ftp.unidata.ucar.edu/pub/netcdf/netcdf-${NETCDF_VERSION}.tar.gz | tar xz && \
    cd netcdf-${NETCDF_VERSION} && \
    ./configure --prefix=/usr/local && make check && make install && ldconfig /usr/local/lib
#RUN cd netcdf-${NETCDF_VERSION} && make check
#RUN cd netcdf-${NETCDF_VERSION} && make install
#RUN ldconfig /usr/local/lib  

###
# Grab and unzip the TDS
###

ENV TDS_VERSION 4.6.13 
ENV TDS_CONTENT_ROOT_PATH /usr/local/tomcat/content

# The amount of Xmx and Xms memory Java args to allocate to THREDDS

ENV THREDDS_XMX_SIZE 1G
ENV THREDDS_XMS_SIZE 1G
ENV THREDDS_WAR_URL https://artifacts.unidata.ucar.edu/repository/unidata-releases/edu/ucar/tds/${TDS_VERSION}/tds-${TDS_VERSION}.war

RUN curl -fSL "${THREDDS_WAR_URL}" -o thredds.war
RUN mkdir -p ${CATALINA_HOME}/webapps/thredds && unzip thredds.war -d ${CATALINA_HOME}/webapps/thredds/

###
# Default thredds config
###
RUN apk add --update ttf-dejavu 

# Install libpng 1.6.25 specifically to avoid an error with a missing symbol inflateValidate
# Using the alpine package repository doesn't allow installation of an older version correctly.
#  - Error relocating /usr/lib/libpng16.so.16: inflateValidate
COPY files/libpng-1.6.25.tar.gz /tmp/libpng-1.6.25.tar.gz
RUN cd /tmp && tar -xvf libpng-1.6.25.tar.gz && cd libpng-1.6.25 && ./configure && \
    make check && make install && ldconfig /usr/local/lib

RUN mkdir -p ${CATALINA_HOME}/content/thredds
COPY files/threddsConfig.xml ${CATALINA_HOME}/content/thredds/threddsConfig.xml

###
# Tomcat users
###

COPY files/tomcat-users.xml ${CATALINA_HOME}/conf/tomcat-users.xml

###
# Tomcat Java Options
###

COPY files/setenv.sh $CATALINA_HOME/bin/setenv.sh
COPY files/javaopts.sh $CATALINA_HOME/bin/javaopts.sh
RUN chmod 755 $CATALINA_HOME/bin/*.sh

###
# Creating .systemPrefs directory according to
# http://www.unidata.ucar.edu/software/thredds/current/tds/faq.html#javaUtilPrefs
# and as defined in the files/javaopts.sh file
###

RUN mkdir -p ${CATALINA_HOME}/javaUtilPrefs/.systemPrefs

###
# CSS and customization 
###
COPY files/oorco_logo.png $CATALINA_HOME/webapps/thredds
COPY files/tds_custom.css $CATALINA_HOME/webapps/thredds
COPY files/tdsCat_custom.css $CATALINA_HOME/webapps/thredds
RUN cat $CATALINA_HOME/webapps/thredds/tds_custom.css >> $CATALINA_HOME/webapps/thredds/tds.css
RUN cat $CATALINA_HOME/webapps/thredds/tdsCat_custom.css >> $CATALINA_HOME/webapps/thredds/tdsCat.css

###
# Cleanup
###
# RUN apk del build-dependencies && rm -rf /var/cache/apk/*
RUN rm -rf /var/cache/apk/*
RUN rm -rf /downloads

WORKDIR ${CATALINA_HOME}

###
# Entrypoint
###
COPY entrypoint.sh /
RUN chmod +x /entrypoint.sh 

###
# Start container
###
ENTRYPOINT ["/entrypoint.sh"]
EXPOSE 8080
CMD ["catalina.sh","run"]
